/*Given the following information about a person:
First Name = Kanye
Last Name = West
Birth Date = 8 June 1977
Annual Income = 150000000.00
Write some code to create an Object with the information above as properties with the appropriate
data type for each property. Once you have created this object, print out the details to the console
*/
let personInfo = {
    FirstName: "Kanye",
    LastName: "West",
    BirthDate: "8 June 1977",
    AnnualIncome: 150000000.00
};
let output = "";
output += personInfo.FirstName +" " + personInfo.LastName + " was born on " + personInfo.BirthDate + " and has an annual income of $" + Math.round(personInfo.AnnualIncome);
console.log(output)